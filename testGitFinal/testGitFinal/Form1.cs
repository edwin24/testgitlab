﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testGitFinal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            try
            {
                validarNombre();
                mostrarMensaje(txtNombre.Text.Trim());
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        public void mostrarMensaje(string mensaje)
        {
            MessageBox.Show(string.Format("Hola {0} ",mensaje));
        }

        public void validarNombre()
        {
            if (txtNombre.Text.Trim().Length == 0)
            {
                throw new Exception("Ingrese el nombre");
            }
        }
    }
}
